# http://sinatrarb.com/intro-ja.html

require 'sinatra'

eval(File.read('pv/swatter.config')) rescue true
@configs ||= {}

require './auth' unless(@configs[:no_auth])

get('/swatter') {
	response['Cache-Control'] = 'no-cache'
	erb :swatter
}

get('/swatter_log') {
	response['Cache-Control'] = 'no-cache'
	erb :swatter_log
}

get('/api/v1/members') {
	erb :rest
}

get('/api/v1/members/:name') {
	erb :rest
}

patch('/api/v1/members/:name') {
	erb :rest
}

get('/qapi/v1/members') {
	erb :qapi
}

get('/qapi/v1/members/:name') {
	erb :qapi
}

get('/*') {
	'?'
}

__END__

