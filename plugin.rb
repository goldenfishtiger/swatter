# coding: utf-8

#-----------------------------------------------------------------------
#
#	pv/plugins/XX-NAME.conf をスキャン
#	pv 上の conf の名前変更で順序制御、削除で無効化ができる
#
def scan_plugins(path = 'plugins')
	Dir.open('pv/%s' % path).sort.each {|entry|
		yield($1, entry) if(entry =~ /^\d+-(.+)\.conf$/)
	}
end

#-----------------------------------------------------------------------
#
#	有効なプラグインの JavaScript をつなげて返す
#
def plug_js(path = 'plugins')

	#-------------------------------------------------------------------
	#
	#	plugins/NAME/FUNC.js を拾集
	#
	jss = []; scan_plugins(path) {|plugin|
		Dir.open('%s/%s' % [path, plugin]).each {|entry|
			if(entry =~ /\.inc\.js$/)
				jss << File.open('%s/%s/%s' % [path, plugin, entry]).read
			end
		}
	}

	<<EOS % jss.join.force_encoding('UTF-8')
		<SCRIPT>
			var plug_js = function(socket, h2s) {
				var callbacks = []
%s
				return(callbacks);
			};
		</SCRIPT>
EOS
end

#-----------------------------------------------------------------------
#
#	有効なプラグインの erb を HTML 化しつなげて返す
#
def plug_erb(path = 'plugins')

	eval(File.read('pv/swatter.config'))

	#-------------------------------------------------------------------
	#
	#	plugins/NAME/XX-FUNC.erb を実行、HTML 化
	#
	htmls = []; scan_plugins(path) {|plugin, entry|
		eval(File.read('pv/%s/%s' % [path, entry]))
		Dir.open('%s/%s' % [path, plugin]).sort.each {|entry|
			if(entry =~ /^\d+-(.+)\.erb$/)
				htmls << erb($1.to_sym)
			end
		}
	}

	htmls.join
end

#-----------------------------------------------------------------------
#
#	swatter_wss が受信したメッセージを処理するプラグインをつなげて返す
#
def plug_wss(path = 'plugins')

	#-------------------------------------------------------------------
	#
	#	plugins/NAME/FUNC_wss を拾集
	#
	scripts = []; scan_plugins(path) {|plugin, entry|
		Dir.open('%s/%s' % [path, plugin]).each {|entry|
			if(entry =~ /_wss$/)
				scripts << File.open('%s/%s/%s' % [path, plugin, entry]).read
			end
		}
	}

	scripts.join
end

#-----------------------------------------------------------------------
#
#	cron プラグインを配列で返す
#
def scan_cron_plugins(path = 'plugins')

	#-------------------------------------------------------------------
	#
	#	plugins/NAME/FUNC.cron を返す
	#
	plugins = []; scan_plugins(path) {|plugin, entry|
		Dir.open('%s/%s' % [path, plugin]).each {|entry|
			if(entry =~ /\.cron$/)
				plugins << '%s/%s/%s' % [path, plugin, entry]
			end
		}
	}

	plugins
end

__END__

