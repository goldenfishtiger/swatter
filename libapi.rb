# coding: utf-8

require 'json'
require 'gdbm'

class Json_result

	def self.member_names(name = nil)
		names = []; if(it = name)
			names << it
		else
			eval(File.read('pv/swatter.config'))
			require './members'
			members = Members.new(@configs)
			members.load
			members.each {|member|
				names << member[:MAVEID]
			}
		end
		names
	end

	def self.members(name = nil)
		data = []; GDBM.open('pv/swatter.gdbm') {|db|
			member_names(name).each {|name|
				entry = {'name' => name}
				re = Regexp.new(/#{name}_(.+)/)
				db.each {|k, v|
					re.match(k.force_encoding('UTF-8')) and entry[$1] = v
				}
				data << entry
			}
		}
		JSON.dump(data)
	end
end

__END__

