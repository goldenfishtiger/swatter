# coding: utf-8

class Time

	def jwday
		%w(日 月 火 水 木 金 土)[self.wday]
	end

	def eweeks
		(self.to_i + 4 * 86400) / (7 * 86400)
	end
end

__END__

