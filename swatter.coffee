$ ->

	socket = new WebSocket($('#wsuri4js').val())

	h2s = (hash) ->
		ss = []; for k, v of hash
			ss.push(":#{k}=>'#{v}'")
		"{#{ss.join(', ')}}"

	# 接続に成功した
	socket.onopen = ->
		socket.send(h2s({ REQUEST: 'login' }) + '@@')

	# 接続に失敗した
	socket.onerror = ->
		$('#status').append("<P><SPAN class='err-state'>Connect error.</SPAN>")
		$('#favicon').remove()
		$('head').append("<LINK rel='icon' id='favicon' href='favicon_d.png' type='image/png'>")

	# 接続がクローズされた(たぶん相手から)
	socket.onclose = ->
		$('#status').append("<P><SPAN class='err-state'>Unexpected disconnect. Try RELOAD.</SPAN>")
		$('#favicon').remove()
		$('head').append("<LINK rel='icon' id='favicon' href='favicon_d.png' type='image/png'>")

	# 名前がクリックされた
	$('.swatter').on('click', '[id^=name_]', ->
		[prefix, name...] = $(@).attr('id').split('_')
		socket.send(h2s({ REQUEST: 'name', NAME: name.join('_') }) + '@@')
	)

	# セレクトボックス(場所)が変更された
	$('.swatter').on('change', '[id^=selectw_]', ->
		[prefix, name...] = $(@).attr('id').split('_')
		socket.send(h2s({ REQUEST: 'selectw', NAME: name.join('_') }) + '@@' + $(@).val())
	)

	# セレクトボックス(状態)が変更された
	$('.swatter').on('change', '[id^=select_]', ->
		[prefix, name...] = $(@).attr('id').split('_')
		socket.send(h2s({ REQUEST: 'select', NAME: name.join('_') }) + '@@' + $(@).val())
	)

	# テキストボックスが変更された
	$('.swatter').on('keydown', '[id^=textarea_]', (event)->
		if(!event.shiftKey and event.keyCode == 13)
			[prefix, name...] = $(@).attr('id').split('_')
			socket.send(h2s({ REQUEST: 'text', NAME: name.join('_') }) + '@@' + $(@).val())
			false
	)

	# メッセージを受信した
	socket.onmessage = (event) ->
		[command, name, value...] = event.data.split(':')
		value = value.join(':')
		if(command == 'where')
			$('#selectw_'	+ name).val(value)
			$('#select_'	+ name).prop('disabled', if value == 'none' then true else false)

		else if(command == 'state')
			$('#name_'		+ name).fadeOut('fast')
			$('#state_'		+ name).removeClass()
			$('#state_'		+ name).addClass(value)
			if(value == 'absent' or value == 'unknown')
				$('#selectw_'	+ name).val('none')
			$('#select_'	+ name).val(value)
			$('#name_'		+ name).fadeIn('fast')

		else if(command == 'note')
			$('#name_'		+ name).fadeOut('fast')
			$('#textarea_' + name).val(value)
			$('#name_'		+ name).fadeIn('fast')

		else if(command == 'update')
			$('#name_'		+ name).attr({ title: value })

		else if(command == 'deactiv')
			$('#name_'		+ name).fadeOut('fast')
			$('#state_'		+ name).removeClass()
			$('#state_'		+ name).addClass(value)
			$('#name_'		+ name).fadeIn('fast')

		else if(command == 'message')
			$('#status').append(value)

		else
			for callback in callbacks
				callback(command, name, value)

	callbacks = plug_js(socket, h2s)

	0

