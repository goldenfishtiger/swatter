# plugin 'noticies'

# 告知フォームを出し入れする
$('#n_toggle').on('click', ->
	$('#n_form').toggle('slow')
)

# 告知ボタンが押された
$('#n_submit').on('click', ->
	$('#n_form').toggle('slow')
	socket.send(h2s({ REQUEST: 'notice' }) + '@@' + $('#n_text').val())
	$('#n_text').val('')
)

callback = (command, name, value) ->
	if(command == 'notice')
		$('#noticies').prepend("<SPAN class='notice'>" + value + "</SPAN><BR>")

callbacks.push(callback)

