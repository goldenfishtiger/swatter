# coding: utf-8

require './TrueLegacyGraphicsCommon'
require 'cairo'
require 'pango'
begin
	require 'rmagick'
rescue ScriptError
	require 'RMagick'
end

#===============================================================================
#
#	Class: LegacyGraphics
#
class LegacyGraphics < TrueLegacyGraphicsCommon

	@@color = []
	pow = { '0' => 0, '1' => 1 }; (0..7).each {|c|				# イロイッカイズツ
		grb = '%03b' % c; @@color << [pow[grb[1, 1]], pow[grb[0, 1]], pow[grb[2, 1]]]
	}

	def self.get_image_size_png(file)
		@@png_image = Magick::Image.read(@@cur_png = file)
		[@@w = @@png_image[0].columns, @@h = @@png_image[0].rows]
	end

	def self.point(x, y)
		return(nil) if(x < 0 or y < 0 or x >= @@w or y >= @@h)
		c = @@png_image[0].pixel_color(x, y)
		[c.red / 65536.0, c.green / 65536.0, c.blue / 65536.0, c.alpha / 65536.0]
	end

	def self.raw_point(x, y)
		return(nil) if(x < 0 or y < 0 or x >= @@w or y >= @@h)
		c = @@png_image[0].pixel_color(x, y)
		[c.red, c.green, c.blue, c.alpha]
	end

	def initialize(h_dmy, w_dmy, y_dmy, x_dmy, gy = 400, gx = 640, cy = 16, cx = 8, c = 0, out = {})
		@gx = gx;	@gy = gy
		@lx = 0;	@ly = 0

		@file = out[:file] || 'TLGCairo'; @ext = out[:type] || 'png'
		@ext =~ /png/i and @ext = 'png' and @surface = Cairo::ImageSurface.new(Cairo::FORMAT_ARGB32, gx, gy)
		@ext =~ /pdf/i and @ext = 'pdf' and @surface = Cairo::PDFSurface.new(    @file + '.' + @ext, gx, gy)
		@ext =~ /svg/i and @ext = 'svg' and @surface = Cairo::SVGSurface.new(    @file + '.' + @ext, gx, gy)
		@ext =~ /ps/i  and @ext = 'ps'  and @surface = Cairo::PSSurface.new(     @file + '.' + @ext, gx, gy)
		@context = Cairo::Context.new(@surface)

		fill(0, 0, gx - 1, gy - 1, c) if(c)
		@context.set_line_width(1)
	end

	#---------------------------------------------------------------------------
	#
	#	_line
	#
	def _line(x1, y1, x2, y2, c, w, d, a)

		@context.set_line_width(w)
		@context.set_source_color(c.class <= Numeric ? @@color[c] : c)
		@context.move_to(x1, y1)
		@context.line_to(x2, y2)
		@context.stroke
	end

	def circle(x, y, r, c, f = '', dy = 1, opt = {})

		@context.set_line_width(opt[:W] || 1)
		@context.set_source_color(c.class <= Numeric ? @@color[c] : c)
		@context.save {
			@context.translate(x, y)
			if(dy < 1.0)
				@context.scale(1.0, dy)
			elsif(dy > 1.0)
				@context.scale(1.0 / dy, 1.0)
			end
			f =~ /f/i and @context.move_to(0, 0)
			@context.arc_negative(0, 0, r, -(opt[:START] || 0), -(opt[:END] || 2 * Math::PI))
			f =~ /f/i and @context.move_to(0, 0)
			f =~ /f/i ? @context.fill : @context.stroke
		}
	end

	def fill(x1, y1, x2, y2, c)

		@context.set_source_color(c.class <= Numeric ? @@color[c] : c)
		@context.rectangle(x1, y1, x2 - x1 + 1, y2 - y1 + 1)
		@context.fill
	end

	def polygon(points, c, f = 'c', w = 1)		# points = [[x1, y1], [x2, y2], [x3, y3]...]

		ps = points.dup
		@context.set_line_width(w)
		@context.set_source_color(c.class <= Numeric ? @@color[c] : c)
		@context.move_to(*(xy = ps.shift))
		@context.line_to(*xy) while(xy = ps.shift)
		f =~ /c/i and @context.close_path
		f =~ /f/i ? @context.fill : @context.stroke
	end

	def curve(x1, y1, x2, y2, x3, y3, x4, y4, c, w = 1)

		@context.set_line_width(w)
		@context.set_source_color(c.class <= Numeric ? @@color[c] : c)
		@context.move_to(x1, y1)
		@context.curve_to(x2, y2, x3, y3, x4, y4)
		@context.stroke
	end

	def put_image_png(x = 0, y = 0, file = @@cur_png, opt = {})

		tx = opt[:TX] || 1.0; ty = opt[:TY] || 1.0
		rx = opt[:RX] || 0.0; ry = opt[:RY] || 0.0; rr = (opt[:RR] || 0.0) * Math::PI / 180
		alpha = opt[:ALPHA] || 1.0

		@context.save {
			@context.clip {
				@context.rectangle(*opt[:CLIP])
			} if(opt[:CLIP])
			@context.translate(x, y)
			@context.scale(tx, ty)
			@context.rotate(rr)
			@context.translate(rx * cos(rr) + ry * sin(rr) - rx, -rx * sin(rr) + ry * cos(rr) - ry)
			@context.set_source(Cairo::ImageSurface::from_png(@@cur_png = file))
			@context.paint(alpha)
		}
	end

	def put_image_png2(x = 0, y = 0, file = @@cur_png, opt = {})

		tx = opt[:TX] || 1.0; ty = opt[:TY] || 1.0
		rx = opt[:RX] || 0.0; ry = opt[:RY] || 0.0; rr = (opt[:RR] || 0.0) * Math::PI / 180

		@context.save {
			@context.translate(x, y)
			@context.scale(tx, ty)
#			@context.rotate(rr)
#			@context.translate(rx * cos(rr) + ry * sin(rr) - rx, -rx * sin(rr) + ry * cos(rr) - ry)
			yield
#			@context.set_source(Cairo::ImageSurface::from_png(@@cur_png = file)) if(file)
#			@context.paint
		}
	end

	def _text(x, y, s, c, ps = {})
#		{ 'font.family' => 'Helvetica', 'font.weight' => 400, 'font.size' => 10, 'layout.width' => 128, 'layout.alignment' => 'left', 'get_size' => true }

		# http://ruby-gnome2.sourceforge.jp/hiki.cgi?Pango%3A%3AFontDescription
		font = Pango::FontDescription.new
		font.family = ps[:font_family] || 'Helvetica'
		font.weight = ps[:font_weight] || Pango::WEIGHT_NORMAL	# WEIGHT_NORMAL: 400, WEIGHT_BOLD: 700
		font.absolute_size = (ps[:font_size] || 10) * Pango::SCALE

		# http://ruby-gnome2.sourceforge.jp/hiki.cgi?Pango%3A%3ALayout
		layout = @context.create_pango_layout
		layout.font_description = font

		layout.width = (ps[:layout_width] || 128) * Pango::SCALE	# x ------- width(dot) ->|
		begin
			layout.alignment = Pango::Alignment::LEFT					# |left....center...right|
			layout.alignment = Pango::Alignment::CENTER		if(it = ps[:layout_alignment] and it =~ /center/i)
			layout.alignment = Pango::Alignment::RIGHT		if(it = ps[:layout_alignment] and it =~ /right/i)
		rescue
			layout.alignment = Pango::Layout::ALIGN_LEFT				# |left....center...right|
			layout.alignment = Pango::Layout::ALIGN_CENTER	if(it = ps[:layout_alignment] and it =~ /center/i)
			layout.alignment = Pango::Layout::ALIGN_RIGHT	if(it = ps[:layout_alignment] and it =~ /right/i)
		end
		layout.text = s

		ps[:get_size] and return(layout.pixel_size)
		@context.set_source_color(c.class <= Numeric ? @@color[c] : c)
		@context.move_to(x, y)
		@context.show_pango_layout(layout)
	end

	def rotate(r = nil, x = 0, y = 0)
		r and @context.translate(x, y) and @context.rotate( rad = r * Math::PI / 180) and @context.translate(-x, -y)
		yield
		r and @context.translate(x, y) and @context.rotate(-rad                     ) and @context.translate(-x, -y)
	end

	def refresh(fps = nil)
		@context.show_page
	end

	def close
		@surface.write_to_png(@file + '.' + @ext) if(@ext == 'png')
		@surface.finish
	end
end

