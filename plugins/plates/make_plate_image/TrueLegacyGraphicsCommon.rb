# coding: utf-8

include Math

def rad(d)
	d * PI / 180.0
end

def deg(r)
	r * 180 / PI
end

module TurtleGraphics

	def tHome
		@t_x = @gx >> 1
		@t_y = @gy >> 1
		@t_ang = 270
		defined?(@t_pen_down) or @t_pen_down = true
		@t_pen_color ||= 0
		@t_pen_width ||= 1
		self
	end

	def tForward(dist)
		x0 = @t_x
		y0 = @t_y
		@t_x = @t_x + cos(rad(@t_ang)) * dist
		@t_y = @t_y + sin(rad(@t_ang)) * dist
		@t_pen_down and _line(x0, y0, @t_x, @t_y, @t_pen_color, @t_pen_width, nil, nil)
	end

	def tBack(dist)
		self.tForward(-dist)
	end

	def tLeft(ang)
		@t_ang -= ang
	end

	def tRight(ang)
		@t_ang += ang
	end

	def tSetPos(x, y)
		@t_x = x
		@t_y = y
	end

	def tSetX(x)
		@t_x = x
	end

	def tSetY(y)
		@t_x = x
	end

	def tSetHeading(ang)
		@t_ang = ang + 270
	end

	def tArc(ang, rad)
		s = ang > 0 ? rad(@t_ang) : rad(@t_ang + ang)
		e = ang > 0 ? rad(@t_ang + ang) : rad(@t_ang)
		circle(@t_x, @t_y, rad, @t_pen_color, '', 1, { :START => s, :END => e})	# 画面右が0度、右回りに描画
	end

	# ang, dist のベクトルで示されるポイントまで、ベジェ曲線を描いて移動する
	# 移動完了時のタートルの向きは ang + th_ang になる
	#	4.times { tThrough(45, 141.421356, 45) } で半径 100 の擬似真円が描画される
	# 制御点までの距離は :SMOOTH で変更可能
	# :DIVS を指定すると、ベジェ曲線のベクトルを分割して返し、描画しない
	#	tThrough(45, 141.421356, 45, { :DIVS => 8 }) {|ang, dist|
	#		tRight(ang); tForward(dist)
	#	} の形で描画できる
	def tThrough(ang, dist, th_ang = 0, opt = {})
		smooth = (it = opt[:SMOOTH]) ? it : [0.55228, 0.55228]
		x1 = (x0 = @t_x) + cos(rad(l_ang = @t_ang )) * dist * smooth[0] / sqrt(2)
		y1 = (y0 = @t_y) + sin(rad(l_ang          )) * dist * smooth[0] / sqrt(2)
		x3 =  x0         + cos(rad(l_ang += ang   )) * dist
		y3 =  y0         + sin(rad(l_ang          )) * dist
		x2 =  x3         - cos(rad(l_ang += th_ang)) * dist * smooth[1] / sqrt(2)
		y2 =  y3         - sin(rad(l_ang          )) * dist * smooth[1] / sqrt(2)
		if(divs = opt[:DIVS])
			divs.is_a?(Integer) and div = divs and divs = [] and (1..div).each {|d|
				divs << 1.0 * d / div
			}
			def _mp(v0, v1, p)
				v0 + (v1 - v0) * p
			end
			divs.each {|p|
				x4 = _mp(x0, x1, p); y4 = _mp(y0, y1, p)
				x5 = _mp(x1, x2, p); y5 = _mp(y1, y2, p)
				x6 = _mp(x2, x3, p); y6 = _mp(y2, y3, p)
				x7 = _mp(x4, x5, p); y7 = _mp(y4, y5, p)
				x8 = _mp(x5, x6, p); y8 = _mp(y5, y6, p)
				x9 = _mp(x7, x8, p); y9 = _mp(y7, y8, p)
				yield(tTowards(x9, y9))
			}
		else
			curve(x0, y0, x1, y1, x2, y2, x3, y3, @t_pen_color, @t_pen_width)
		end
		@t_x = x3; @t_y = y3; @t_ang = l_ang
	end

	def tPos
		[@t_x, @t_y]
	end

	def tPosition
		self.tPos
	end

	def tHeading
		(@t_ang - 270) % 360
	end

	def tXpos
		@t_x
	end

	def tYpos
		@t_y
	end

	def tTowards(x, y)
		[(deg(atan2((dy = y - @t_y), (dx = x - @t_x))) - @t_ang) % 360, sqrt(dx ** 2 + dy ** 2)]
	end

	# 描画距離に合うよう、破線パターンを補正
	#	pat = [開始ポインタ, [描画距離, 間隔距離, 描画距離, 間隔距離, ...]]
	def fit_dline(dist, pat, opt = {})
		sum = 0.0; npat1 = pat[1].size; n = -1; loop { n += 1
			sum += pat[1][(pat[0] + n) % npat1]
			if(opt[:FIT])
				n % npat1 == 0 or next
			else
				n % npat1 == npat1 - 1 or next
			end
			sum > dist + (opt[:PDIST] || 0) and break
		}
		cpat = []; pat[1].each {|l| cpat << l * dist / sum }
		[dist, [pat[0], cpat], opt]
	end

	def _tDForward(dist, pat, step, opt = {})
		rest = dist; while(rest > step)
			step > 0 and (it = opt[:AFORWARD]) ? it.call(step, pat, opt) : self.tForward(step)
			rest -= step
			step = pat[1][(pat[0] += 1) % pat[1].size]
			pat[0] % 2 == 0 ? self.tPenDown : self.tPenUp
		end
		(it = opt[:AFORWARD]) ? it.call(rest, pat, opt) : self.tForward(rest)
		step - rest
	end

	# 指定の破線パターンで直線を描画
	def tDForward(dist, pat, opt = {})
		step = pat[1][pat[0]]
		pat[0] % 2 == 0 ? self.tPenDown : self.tPenUp
		self._tDForward(dist, pat, step, opt)
	end

	def tPush
		@t_stack ||= []
		@t_stack << [@t_x, @t_y, @t_ang]
	end

	def tPop
		@t_x, @t_y, @t_ang = @t_stack.pop
	end

	def tPushStat
		@t_sstack ||= []
		@t_sstack << [@t_pen_down, @t_pen_color, @t_pen_width]
	end

	def tPopStat
		@t_pen_down, @t_pen_color, @t_pen_width = @t_sstack.pop
	end

	def tHideTurtle
		true
	end

	def tShowTurtle
		true
	end

	def tSetTurtle(n)
		raise('not imprement yet.')
	end

	def tPenDown
		@t_pen_down = true
	end

	def tPenUp
		@t_pen_down = false
	end

	def tSetPenColor(n)
		@t_pen_color = n
	end

	def tFill
		raise('not imprement yet.')
	end

	def tSetPenWidth(n)
		@t_pen_width = n
	end

	def tSetLineDash
		raise('not imprement yet.')
	end

	def tPen
		[@t_pen_down ? :DOWN : :UP, @t_pen_color]
	end

	def tFont
		raise('not imprement yet.')
	end
end

#===============================================================================
#
#	Class: TrueLegacyGraphicsCommon
#
class TrueLegacyGraphicsCommon

	include TurtleGraphics

	attr_reader :gx, :gy
	attr_reader :sprites

	def initialize(h_dmy, w_dmy, y_dmy, x_dmy, gy = 400, gx = 640, cy = 16, cx = 8, c = 0, out = {})
		raise('method MAST overload.')			# define @gx, @gy
	end

	def fill(x1, y1, x2, y2, c)
		raise('method MAST overload.')
	end

	def _line(x1, y1, x2, y2, c, w, d, a)
		raise('method MAST overload.')
	end

	def polygon(points, c, f = '', w = 1)		# points = [[x1, y1], [x2, y2], [x3, y3]...]
		raise('method MAST overload.')
	end

	def curve(x1, y1, x2, y2, x3, y3, x4, y4, c, w = 1)
		raise('method MAST overload.')
	end

	def pset(x, y, c)
		fill(x, y, x, y, c)
	end

	def line(x1, y1, x2, y2, c, bf = '', w = 1, d = nil, a = nil)
		if(bf =~ /f/i)
			fill(x1, y1, x2, y2, c)
		elsif(bf =~ /b/i)
			polygon([[x1, y1], [x2, y1], [x2, y2], [x1, y2]], c, 'c', w)
		else
			@lx = x2
			@ly = y2
			_line(x1, y1, x2, y2, c, w, d, a)
		end
	end

	def cline(x, y, c, w = 1, d = nil, a = nil)
			  x1 = @lx
			  y1 = @ly
		@lx = x2 = x
		@ly = y2 = y
		_line(x1, y1, x2, y2, c, w, d, a)
	end

	def circle(x, y, r, c, f = '', dy = 1, opt = {})
		raise('method MAST overload.')
	end

	def put_image_png(x, y, file, opt)
		raise('method MAST overload.')
	end

	def text(x, y, s, c, ps = {})
		ps[:font_family] =~ /\.bmf$/ ?  _text_bmf(x, y, s, c, ps) : _text(x, y, s, c, ps)
	end

	def _text(x, y, s, c, ps = {})
		raise('method MAST overload.')
	end

	def locate(x, y)
		@tx = x * 8; @ty = y * 16
	end

	def print(mes, c = 7)
		text(@tx, @ty, mes, c)
	end

	def _text_bmf(x, y, s, c, ps = {})
		__text_bmf(x, y, s, c, ps) {}
	end
	def __text_bmf(x, y, s, c, ps)
#		{ 'font.family' => 'Helvetica', 'font.weight' => 400, 'font.size' => 10, 'layout.width' => 128, 'layout.alignment' => 'left', 'get_size' => true }

		@fonts ||= {}
		@fonts[font = ps[:font_family] || 'default.bmf'] ||= File.open(font)
		d8_3 = ps[:font_d16] ?  4 : 3; d8_8 = ps[:font_d16] ? 16 : 8

		(it = ps[:font_size]) and (tx = it >> d8_3; ty = it >> d8_3)	# 8 -> 8dot
		tx ||= ps[:tx] || 1; ty ||= ps[:ty] || 2; raster = ps[:raster] || 0	# depend :ty

		wpad = 0; fsize = [s.size * tx * d8_8, ty * d8_8]
		ps[:get_size] and return(fsize)
		ps[:layout_alignment] =~ /right/i  and wpad =  ps[:layout_width] - fsize[0]
		ps[:layout_alignment] =~ /center/i and wpad = (ps[:layout_width] - fsize[0]) / 2

		pset_func = ps[:PSET_FUNC] || method(:fill).to_proc

		cx = 0; cy = 0; s.each_byte {|code|
			if(code < 0x20 and !ps[:vcode])
				code == 0x0a and (cx = 0; cy += 1; next)
				code =  0x20
			end
			@fonts[font].seek(code * (it = ps[:font_d16] ? 32 : 8))
			bps = @fonts[font].read(it)
			ry = 0; bps.unpack(ps[:font_d16] ? 'n*' : 'C*').each {|bp|
				ps[:reverse] and bp ^= (ps[:font_d16] ? 0xFFFF : 0xFF)
				rx = 0; while(bp != 0)
					px = (cx * d8_8 + rx) * tx; py = (cy * d8_8 + ry) * ty
					bp & (ps[:font_d16] ? 0x8000 : 0x80) != 0 and yield(pset_func.call(wpad + x + px, y + py, wpad + x + px + tx - 1, y + py + ty - 1 - raster, c))
					bp = (bp << 1) & (ps[:font_d16] ? 0xFFFF : 0xFF)
					rx += 1
				end
				ry += 1
			}
			cx += 1
		}
	end

	def symbol(x, y, s, tx, ty, c, ps = {})
		ps[:tx] ||= tx; ps[:ty] ||= ty
		_text_bmf(x, y, s, c, ps)
	end

	def get(x, y, xn, yn, file, xs = 1, ys = 1)
		xy = LegacyGraphics.get_image_size_png(file)
		x ||= 0; y ||= 0; xn ||= xy[0]; yn ||= xy[1]
		pat = []; (0...yn).each {|ry|
			hpat = []; (0...xn).each {|rx|
				c = LegacyGraphics.point(x + rx * xs, y + ry * ys)
				hpat << ((it = yield(c)) ? it : (c.opacity > 0 ? 9 : 0))
			}
			pat << hpat
		}
		pat
	end

	def put(x, y, pat, opt = {})

		(it = opt[:dot_size]) and tx = ty = it
		tx ||= opt[:tx] || 2; ty ||= opt[:ty] || 2; raster = opt[:raster] || 0	# depend :ty
		wpad = 0; fsize = [pat[0].size * tx, pat.size * ty]
		opt[:get_size] and return(fsize)
		opt[:layout_alignment] =~ /right/i  and wpad =  opt[:layout_width] - fsize[0]
		opt[:layout_alignment] =~ /center/i and wpad = (opt[:layout_width] - fsize[0]) / 2

		pset_func = opt[:PSET_FUNC] || method(:fill).to_proc

		cx = 0; cy = 0
		ry = 0; pat.each {|hpat|
			rx = 0; hpat.each {|c|
				px = (cx * 8 + rx) * tx; py = (cy * 8 + ry) * ty
				c < 8 and pset_func.call(wpad + x + px, y + py, wpad + x + px + tx - 1, y + py + ty - 1 - raster, c)
				rx += 1
			}
			ry += 1
		}
	end

	def rotate(r = nil, x = 0, y = 0)
		raise('not implement yet.')
	end

	def _color(c, color)
		c.is_a?(Numeric) and return(color[c])
		c.is_a?(Array) and return(c)
		return(color[7])
	end

	def clear
	end

	def box(vchar_dmy, hchar_dmy)
	end

	def refresh(fps = nil)
		raise('method MAST overload.')
	end

	def getch
	end

	def close
	end

	def init_sprites
		@sprites = Sprites.new(self)
	end

	def method_missing(func, *args)
		func =~ /3d$/ and return
		raise
	end
end

class Sprite

	def initialize(win)
		@win = win
	end

	def fill(x1, y1, x2, y2, c)
		@win.fill(x1, y1, x2, y2, c)
	end

	def line(x1, y1, x2, y2, c, bf = '', w = 1, d = nil, a = nil)
		@win.line(x1, y1, x2, y2, c, bf, w, d, a)
	end

	def polygon(vertexes, c, f = '', w = 1)
		@win.polygon(vertexes, c, f, w)
	end

	def circle(x, y, r, c, f = '', dy = 1, opt = {})
		@win.circle(x, y, r, c, f, dy, opt)
	end

	def text(x, y, s, c, ps = {})
		@win.text(x, y, s, c, ps)
	end

	def draw
	end

	def method_missing(func, *args)
		func =~ /3d$/ and return
		raise
	end
end

class Sprites < Hash

	include TurtleGraphics

	def initialize(win)
		@win = win
		@gx = win.gx; @gy = win.gy
		@seq = '_@'
	end

	def pick(key)
		self[key] ||= Sprite.new(@win)
	end

	def _line(x1, y1, x2, y2, c, w, d, a)
		sprite = pick(@seq.succ!)
		sprite.line(x1, y1, x2, y2, c, '', w, d, a)
	end

	def draw
	end
end

__END__

