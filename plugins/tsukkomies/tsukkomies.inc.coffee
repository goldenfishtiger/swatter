# plugin 'tsukkomies'

$('#t_toggle').on('click', ->
    $('#past_tsukkomies').toggle('slow')
    $('#t_form').toggle('slow')
)

$('.swatter').on('click', '[id^=icon_]', ->
    $('#past_tsukkomies').toggle('slow')
    $('#t_form').toggle('slow')
    socket.send(h2s({ REQUEST: 'tsukkomi' }) + '@@' + $(@).html() + $('#t_text').val())
    $('#t_text').val('')
)

callback = (command, name, value) ->
	if(command == 'tsukkomi')
		t1 = $('#tsukkomi_1').html()
		t0 = $('#tsukkomi_0').html()
		$('#past_tsukkomies').append("<BR><SPAN class='tsukkomi'>" + t1 + "</SPAN>")
		$('#tsukkomi_1').html(t0)
		$('#tsukkomi_0').html(value)

callbacks.push(callback)

