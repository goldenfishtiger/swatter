# coding: utf-8

=begin

* members.rb

	メンバ一覧

=end

require 'net/ldap'

class Members

	attr_reader :status

	def initialize(configs)
		@configs = configs
		@members = []
		@name2uid = {}
		@status = false
	end

	def load
		begin
			ldap = Net::LDAP.new(
				host:	@configs[:ldap_host],
				port:	@configs[:ldap_port],
			)
			users = ldap.search(
				filter:		'(listOrder=*)',
				base:		@configs[:ldap_search_base],
			)
			uid_gidnums = {}; users.each {|user|				# {"furuta"=>{"201"=>true, "202"=>true, "299"=>true}, ...}
				uid_gidnums[user['uid'][0]] = { user['gidnumber'][0] => true }
			}
			target_gidnums = nil; if(it = @configs[:ldap_group_search_base])
				groups = ldap.search(
					filter:		'(cn=*)',
					base:		it,
				)
				target_gidnums = []; groups.each {|group|		# ["201", "202"]
					group['memberuid'].each {|uid|
						uid_gidnums[uid][group['gidnumber'][0]] = true
					}
					@configs[:ldap_target_groups].each {|gid|
						gid == group['cn'][0] and target_gidnums << group['gidnumber'][0]
					}
				}
			end
			users.sort {|key_a, key_b|
				-(key_a['listOrder'][0].to_i <=> key_b['listOrder'][0].to_i)
			}.each {|user|
				hide = false; if(target_gidnums)
					hide = true; target_gidnums.each {|gidnum|
						uid_gidnums[user['uid'][0]][gidnum] and hide = false
					}
				end
				hide and next
				@members << {
					:UID	=> (u = user['uid'][0]),			# ユーザ名(数値ではない)
					:LORDER	=>		user['listOrder'][0].to_i,
					:MAVEID	=> (n = user['maveid'][0].force_encoding('UTF-8')),
					:MAIL	=>		user['mail'][0],
				}
				@name2uid[n] = u
			}
			@status = true
		rescue
		end if(@configs[:ldap_host])

		begin
			names = []; File.open('pv/member_list', 'r:UTF-8') {|fh|
				fh.each {|name|
					name =~ /^#/ and next
					(it = name.strip).size > 0 and names << it
				}
			}
			lorder = names.size + 32; names.each {|name|
				@members << {
					:UID	=> (u = name),
					:LORDER	=>		lorder -= 1,
					:MAVEID	=> (n = name),
					:MAIL	=>		'',
				}
				@name2uid[n] = u
			}
		rescue
		end

		lorder = 16; @names = [
			'金剛',
			'比叡',
			'榛名',
			'霧島',
			'扶桑',
			'山城',
			'伊勢',
			'日向',
			'長門',
			'陸奥',
			'大和',
			'武蔵',
		].each {|name|
			@members << {
				:UID	=> (u = name),
				:LORDER	=>		lorder -= 1,
				:MAVEID	=> (n = name),
				:MAIL	=>		'',
			}
			@name2uid[n] = u
		} if(size < 1)
	end

	def reorder(weight)
		@members.each {|member|
			(it = weight.shift) or break
			member[:LORDER] += (it == -128 ? -99999 : it)
		}
		@members.sort! {|member_a, member_b|
			-(member_a[:LORDER] <=> member_b[:LORDER])
		}
	end

	def size
		@members.size
	end

	def each(min = 0)
		@members.each {|member|
			yield(member) if(member[:LORDER] > min)
		}
	end

	def each_uid(min = 0)
		each(min) {|member|
			yield(member[:UID])
		}
	end

	def each_name(min = 0)
		each(min) {|member|
			yield(member[:MAVEID])
		}
	end

	def names(min = 0)
		names = []; each(min) {|member|
			names << member[:MAVEID]
		}
		names
	end

	def name2uid(name)
		@name2uid[name]
	end
end

__END__

